#### What
[AptCacherNg](https://wiki.debian.org/AptCacherNg) is a nice little software to create a local cache of the Debian mirrors. This git repo dockerize the AptCacherNg project.

###### Run with Prebuilt docker image
```shell
docker run --it -d -p 3142:3142 -e NG_ADMIN_USER=admin -e NG_ADMIN_PASSWORD=secure touhid/apt-ng-cacher-docker:latest
```

#### BUILD your own
```shell
docker build --no-cache -t apt-ng-cacher-docker .
```

#### Run
```shell
docker run --it -d -p 3142:3142 apt-ng-cacher-docker
```
Report (/acng-report.html) Admin default <code>Username:</code>ng_admin_user <code>Password:</code>ng_web_password.
To change username password use <code>NG_ADMIN_USER</code> <code>NG_ADMIN_PASSWORD</code> environment variables.
For ex:
```shell
docker run --it -d -p 3142:3142 -e NG_ADMIN_USER=admin -e NG_ADMIN_PASSWORD=secure apt-ng-cacher-docker
```

