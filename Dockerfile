# https://help.ubuntu.com/community/Apt-Cacher%20NG
FROM ubuntu:jammy-20230425
# ex: https://github.com/mbentley/docker-apt-cacher-ng/blob/master/Dockerfile
# ex: https://github.com/sameersbn/docker-apt-cacher-ng/blob/master/Dockerfile
MAINTAINER thisistouhid@gmail.com

ENV APT_CACHER_NG_VERSION=3.7.4-1build1
ENV APT_CACHER_NG_CACHE_DIR=/var/cache/apt-cacher-ng
ENV APT_CACHER_NG_LOG_DIR=/var/log/apt-cacher-ng
ENV NG_ADMIN_USER=ng_admin_user
ENV NG_ADMIN_PASSWORD=ng_web_password

VOLUME ["${APT_CACHER_NG_CACHE_DIR}"]
RUN mkdir -p ${APT_CACHER_NG_CACHE_DIR}

RUN apt-get update -y ; \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y apt-cacher-ng=${APT_CACHER_NG_VERSION} \
    curl ca-certificates vim wget; \
    apt-get clean all; \
    rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* ;

RUN sed 's/# ForeGround: 0/ForeGround: 1/' -i /etc/apt-cacher-ng/acng.conf && \
    sed "s/# ReuseConnections: 1/ReuseConnections: 1/g" -i /etc/apt-cacher-ng/acng.conf && \
    sed 's/# PassThroughPattern:.*this would allow.*/PassThroughPattern: .* #/' -i /etc/apt-cacher-ng/acng.conf; \
    echo "AdminAuth: ng_admin_user:ng_web_password" >> /etc/apt-cacher-ng/acng.conf;

EXPOSE 3142

HEALTHCHECK --interval=10s --timeout=2s --retries=3 \
    CMD wget -q -t1 -O /dev/null  http://localhost:3142/acng-report.html || exit 1

CMD sed "s/AdminAuth:.*/AdminAuth: ${NG_ADMIN_USER}:${NG_ADMIN_PASSWORD}/" -i /etc/apt-cacher-ng/acng.conf && \
    chmod -R 777 ${APT_CACHER_NG_CACHE_DIR} && \
    /etc/init.d/apt-cacher-ng start && \
    tail -f ${APT_CACHER_NG_LOG_DIR}/*

